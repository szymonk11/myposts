<?php
namespace MyPosts\Libraries;

/**
 * Klasa odpowiedzialna za połączenie z bazą danych i wykonywanie zapytań
 */
class DatabaseOperator
{
    private static $instance = NULL;

    private $conn = NULL;

    protected function __construct() 
    {
        $config = new \PHLAK\Config\Config('config.yaml');

        $this->conn = new \mysqli(
            $config->get('db_host'), 
            $config->get('db_user'), 
            $config->get('db_password'), 
            $config->get('db_database')
        );
    }

    protected function __clone() { }
    public function __wakeup()
    {
        throw new \Exception("Cannot unserialize a singleton.");
    }

    public static function getInstance(): DatabaseOperator
    {
        if (self::$instance === NULL) {
            self::$instance = new DatabaseOperator();
        }

        return self::$instance;
    }

    public function fetch_all_from(string $tablename): array
    {
        $q = $this->conn->query(sprintf(
            "SELECT * FROM %s", 
            $this->escape_tablename($tablename)
        ));

        return $q->fetch_all(MYSQLI_ASSOC) ?? [];
    }

    public function get_one(string $tablename, int $id, string $whereColumn = 'id'): array
    {
        $q = $this->conn->query(sprintf(
            "SELECT * FROM %s WHERE %s = %d", 
            $this->escape_tablename($tablename),
            $this->escape_columname($whereColumn),
            $this->escape_integer($id)
        ));

        return $q->fetch_assoc() ?? [];
    }

    public function update(string $tablename, array $data, int $id, string $whereColumn = 'id'): bool
    {
        $q = sprintf("UPDATE %s SET ", $this->escape_tablename($tablename));

        foreach($data as $col => $val) {
            $q .= $this->escape_columname($col) ." = '". $this->escape_string($val) ."',";
        }

        $q = substr($q, 0, -1);

        $q .= ' WHERE ' . $this->escape_columname($whereColumn) .' = '. $this->escape_integer($id);

        return $this->conn->query($q) ? true : false;
    }

    public function insert(string $tablename, array $data): int
    {
        $columns = [];
        $values = [];

        foreach($data as $c => $v) {
            $columns[] = $this->escape_columname($c);
            $values[] = "'". $this->escape_string($v) ."'";
        }

        $q = $this->conn->query(sprintf(
            "INSERT INTO %s (%s) VALUES (%s)", 
            $this->escape_tablename($tablename),
            join(',', $columns),
            join(',', $values)
        ));
        
        echo sprintf(
            "INSERT INTO %s (%s) VALUES %s", 
            $this->escape_tablename($tablename),
            join(',', $columns),
            join(',', $values)
        );

        return $this->conn->insert_id;
    }

    public function delete(string $tablename, int $id, string $whereColumn = 'id'): bool
    {
        $q = sprintf("DELETE FROM %s WHERE %s = %d ", 
            $this->escape_tablename($tablename),
            $this->escape_columname($whereColumn),
            $this->escape_integer($id)
        );

        return $this->conn->query($q) ? true : false;
    }


    private function escape_tablename(string $tablename): string
    {
        return $this->conn->real_escape_string($tablename);
    }

    private function escape_columname(string $column): string
    {
        return $this->conn->real_escape_string($column);
    }

    private function escape_string(string $value): string
    {
        return $this->conn->real_escape_string($value);
    }

    private function escape_integer(int $value): int
    {
        return $value;
    }
}