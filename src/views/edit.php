<?php get_header(); ?>

<h1>Edytuj artykuł</h1>

<?php
if(isset($success)) {
?>
    <div class="alert alert-success" role="alert"><?= $success; ?></div>
<?php
}

$form->open();

$form->hidden('id', $post['id']);

$form->text([
    'name' => 'title',
    'label' => 'Tytuł artykułu',
    'id' => 'title',
    'value' => $post['title']
]);

$form->textarea([
    'name' => 'content',
    'label' => 'Treść artykułu',
    'id' => 'content',
    'value' => $post['content']
]);

$form->select([
    'name' => 'status',
    'label' => 'Status artykułu',
    'id' => 'status',
    'options' => [
        1 => 'Opublikowany',
        2 => 'Ukryty'
    ],
    'selected' => $post['status'],
    'value' => $post['status']
]);

echo '<br /><br />';

$form->input_submit('button', '', 'Zapisz zmiany');

$form->close();
?>

<?php get_footer(); ?>