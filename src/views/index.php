<?php get_header(); ?>

<h1>Lista artykułów</h1>

<ul>
<?php foreach($posts as $post): ?>
    <li><a href="/edit/<?= $post['id']; ?>">#<?= $post['id']; ?></a> - <?= $post['title']; ?> - <a class="text-danger" href="/delete/<?= $post['id']; ?>">Usuń</a></li>
<?php endforeach; ?>
</ul>

<?php get_footer(); ?>