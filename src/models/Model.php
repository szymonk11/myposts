<?php
namespace MyPosts\Models;

/**
 * Klasa nadrzędna dla modeli
 */
abstract class Model
{    
    /**
     * Obiekt operatora bazy danych
     *
     * @var \MyPosts\Libraries\DatabaseOperator
     */
    protected $database;
    
    /**
     * Metoda ładująca operator bazy danych do pola
     *
     * @return void
     */
    protected function loadDatabase(): void
    {
        $this->database = \MyPosts\Libraries\DatabaseOperator::getInstance();
    }
}
