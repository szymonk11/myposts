<?php 
namespace MyPosts\Models;

class PostsModel extends Model
{
    public function __construct() {
        $this->loadDatabase();
    }

    public function get(int $postId): array
    {
        return $this->database->get_one('posts', $postId);
    }

    public function getAll(): array
    {
        return $this->database->fetch_all_from('posts');
    }

    public function update(int $postId, array $data): bool
    {
        return $this->database->update('posts', $data, $postId);
    }

    public function add(array $data): int
    {
        return $this->database->insert('posts', $data);
    }

    public function delete(int $postId): void
    {
        $this->database->delete('posts', $postId);
    }
}