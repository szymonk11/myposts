<?php
namespace MyPosts\Controllers;

/**
 * Klasa nadrzędna dla kontrolerów
 */
abstract class Controller
{
    /**
     * Pole z danymi przekazywanymi do widoków
     *
     * @var array
     */
    protected array $data = [];

    /**
     * Metoda wyświetlająca podany widok (plik z HTML)
     *
     * @param  string $template Nazwa widoku
     * @return void
     */
    protected function template(string $template): void
    {
        extract($this->data);
        include __DIR__ . "./../views/$template.php";
    }
    
    /**
     * Metoda przekierowująca do innej ścieżki
     *
     * @param  string $path Ścieżka przekierowania
     * @return void
     */
    protected function redirect(string $path): void
    {
        header("Location: " . $path);
        die();
    }
}