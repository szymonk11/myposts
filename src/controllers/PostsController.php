<?php
namespace MyPosts\Controllers;

class PostsController extends Controller
{
    private $model;

    public function __construct() {
        $this->model = new \MyPosts\Models\PostsModel;
    }

    public function getIndex(): void
    {   
        $this->data['posts'] = $this->model->getAll();

        $this->template('index');
    }

    public function anyEdit(int $id): void
    {
        $post = $this->model->get($id);
        if(empty($post)) {
            $this->redirect('/');
        }

        $form = new \Formr\Formr('bootstrap');
        $form->action = '/edit/' . $id;
        
        if ($form->submitted())
        {
            $data = $form->validate('title(required|min[6]|max[128]), content(required|min[6]), status(integer)');

            if($form->ok()) {
                $update = $this->model->update($id, $data);
                if($update) {
                    $this->data['success'] = 'Dane zostały pomyślnie zapisane.';
                }
            }
        }

        $this->data['form'] = $form;
        $this->data['post'] = $post;
        $this->template('edit');
    }

    public function anyAdd(): void
    {
        $form = new \Formr\Formr('bootstrap');
        $form->action = '/add';
        

        $post = [
            'title' => '',
            'content' => '',
            'status' => ''
        ];
        if ($form->submitted())
        {
            $post = $form->validate('title(required|min[6]|max[128]), content(required|min[6]), status(integer)');

            if($form->ok()) {
                $add = $this->model->add($post);
                if($add) {
                    $this->redirect('/');
                }
            }
        }

        $this->data['form'] = $form;
        $this->data['post'] = $post;
        $this->template('add');
    }

    public function anyDelete(int $id): void
    {
        $post = $this->model->get($id);
        if(empty($post)) {
            $this->redirect('/');
        }

        $form = new \Formr\Formr('bootstrap');
        $form->action = '/delete/' . $id;

        if ($form->submitted()) {
            $this->model->delete($id);
            $this->redirect('/');
        }

        get_header();
        $form->open();
        $form->info_message("Czy na pewno chcesz usunąć artykuł #$id?");
        $form->input_submit('submit', '', 'Tak, usuwam', '', 'class="btn btn-danger"');
        echo '<br /><a href="/" class="btn btn-secondary">Powrót</a>';
        $form->close();
        get_footer();
    }
    
}