<?php
/**
 * Funkcje pomocnicze dla template'ów
 */

if(!function_exists('get_header')) {
    function get_header()
    {
        include  __DIR__ . '/../views/layout/header.php';
    }
}

if(!function_exists('get_footer')) {
function get_footer()
    {
        include  __DIR__ . '/../views/layout/footer.php';
    }
}
