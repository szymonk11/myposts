<?php

// composer
require_once __DIR__ . '/vendor/autoload.php';

// source
function loadFromDir(string $dir)
{
    foreach (glob("$dir/*.php") as $filename)
    {
        require_once $filename;
    }
}

loadFromDir(__DIR__ . '/src/helpers');
loadFromDir(__DIR__ . '/src/libraries');
loadFromDir(__DIR__ . '/src/controllers');
loadFromDir(__DIR__ . '/src/models');
