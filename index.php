<?php
require_once __DIR__ . '/autoload.php';

use Phroute\Phroute\RouteCollector;
use Phroute\Phroute\Dispatcher;

$router = new RouteCollector();
$router->controller('', 'MyPosts\\Controllers\\PostsController');

$dispatcher = new Phroute\Phroute\Dispatcher($router->getData());

try {
    $dispatcher->dispatch($_SERVER['REQUEST_METHOD'], parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
} 
catch (Exception $e) 
{
    if($e instanceof Phroute\Phroute\Exception\HttpRouteNotFoundException)
    {
        http_response_code(404);
        echo '<h1>Nie odnaleziono strony</h1>';
        die();
    }
}

    
