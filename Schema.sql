--
-- Host: 127.0.0.1
-- Czas generowania: 20 Gru 2021, 18:38
-- Wersja serwera: 10.4.10-MariaDB
-- Wersja PHP: 7.4.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `myposts`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(128) NOT NULL,
  `content` text NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `posts`
--

INSERT INTO `posts` (`id`, `title`, `content`, `status`) VALUES
(1, 'Artykuł 1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed porta, nunc non ullamcorper porta, metus ligula euismod arcu, vel porttitor lorem lacus vulputate massa. Nulla sit amet nunc vel ligula pretium commodo. Nam eget urna ex. Proin rhoncus aliquam odio, at porttitor est porta eget. Suspendisse dapibus, nisi vitae feugiat porttitor, nisl felis ultrices dui, in sodales risus elit sed dui. Etiam diam magna, consequat in venenatis quis, euismod bibendum quam. Morbi interdum est non arcu dictum hendrerit. ', 1),
(2, 'Artykuł inny', 'Treść treść...', 1);

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT dla tabel zrzutów
--

--
-- AUTO_INCREMENT dla tabeli `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
